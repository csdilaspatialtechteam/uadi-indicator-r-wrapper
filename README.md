# README #

This is a sample project for creating UADI indicator tools using R.

### DevLogs (2017-09-07)###

Version 2.7 (uadi_utils.R):

(1) add "iscategoricalattr" parameter to methods: utils.publishGeolayer, utils.publishGeolayerWithMultipleStyles, utils.createWMSStyle, utils.createWFSStyle. Now style can be created for categorical values.

(2) revise methods: utils.isValidColorName, utils.isValidColorNum, utils.isValidClassifier, now multiple valid values provided in one string can be parsed. e.g. "Reds,Blues", "5,5", "Jenks,Jenks" 



### DevLogs (2017-08-31)###

Version 1.2 (uadi_indicator_greenarea.R):

(1) Charts visualisation improvements with the support configuration parameters: width, height, xaxistitle, yaxistitle, xaxislabelsuffix, yaxislabelsuffix



### DevLogs (2017-08-15)###

Version 2.6 (uadi_utils.R):

(1) add utils.project2WGS84 method

### DevLogs (2017-08-12)###

Version 2.5 (uadi_utils.R):

(1) add three new functions to verify style related inputs: utils.isValidClassifier, utils.isValidColorNum, utils.isValidColorName



### DevLogs (2017-07-19)###

Version 2.4 (uadi_utils.R):

(1) a new function "utils.loadGeoJSON2DF" for handling null geometry geojson urls.

(2) "utils.createWorkspace" and "utils.getWorkspace" functions are created to manipulate workspaces.

(3) a temporay fix for the glitch when calling "utils.createFeatureType" function. In some scenarios, the rest api raises a 500 error while the datalayer can still be published successfully in geoserver. just skip setting procflag = FALSE to make it bit more robust and tolerable.

Version 1.1 (uadi_indicator_greenarea.R):

(1) Implement the mus greenarea calculation in parallel computation. the original normal loop method is commented



### DevLogs (2017-06-22)###

Version 2.3 (uadi_utils.R):

(1) use a new function "utils.publishGeolayerWithMultipleStyles" to publish one geolayer with multiple styles. This function is particularly useful when developers need to render various attribute of a single output geolayer.

Version 1.0 (uadi_indicator_greenarea.R):

(1) create a fully functional sample script for greenarea indicator.



### DevLogs (2017-06-16)###

Version 2.2 (uadi_utils.R):

(1) use a new function "utils.publishGeolayer" to combine geolayer publish and styling in one place. it makes sp object publishing a bit easier.



### DevLogs (2017-06-09)###

Version 2.1 (uadi_utils.R):

(1) fix bug (null is returned when warning occurs in "utils.loadGeoJSON2SP" method). A valid sp object will be returned and warning messages will print out.

(2) add timeout (36000 seconds) for "utils.addShp2DataStore" and "utils.loadGeoJSON2SP" methods.



### Get a devKey for your development ###

Please contact University of Melbourne Dev Team to get a valid devKey. This devKey is crucial for interacting data with UADI system. 


### Quick summary ###

This project contains sample codes for developing UADI indicators in R. 

"uadi_indicator_sample.R" is the main source code you need to investigate and implement.

"uadi_utils.R" contains all utility functions to make the development as simple as possible. 

When registering indicator in UADI dashboard, ONLY "uadi_indicator_YOUR_SCRIPT_NAME.R" can be uploaded. Given this, the "uadi_utils.R" file should be treated as a static helper script and any changes in it will not be used in the UADI.

All codes are tested in R 3.3 environment on windows and linux.

To check available packages or add new packages in UADI product environment, please visit:　

http://apps.csdila.ie.unimelb.edu.au/uadi/rpackages.html

Adding packages can be tricky, particularly when extral libraries are prerequisite. Please contact UoM dev team for any questions.


### How do I get set up? ###

A couple of softwares and tools are required to create UADI indicators usring R:

(1) Install R (version 3.3 or above) from the official website

https://cran.r-project.org/


(2) RStudio is recommended for developing R scripts. The "RStudio Desktop Open Source License" version just works great.

https://www.rstudio.com/products/rstudio/download/


(3) Install Rtools (for Windows developers only) from here 

https://cran.r-project.org/bin/windows/Rtools/ 

since zip.exe in the Rtools packages will be used in the "uadi_utils.R", please make sure "YOUR_Rtools_DIRECTORY/bin" is included in the system "Path" environment variable.

(4) When testing your indicator script, RScript.exe which locates in "YOUR_R_DIRECTORY\bin" will be used, make sure this directory is included in the system "Path" environment variable.

(5) Download the source code from 

https://bitbucket.org/csdilaspatialtechteam/uadi-indicator-r-wrapper/downloads/

or make a git clone (recommended) from 

https://bitbucket.org/csdilaspatialtechteam/uadi-indicator-r-wrapper.git

(6) Change file name "uadi_indicator_sample.R" to "uadi_indicator_YOUR_SCRIPT_NAME.R".
 
(7) Contact the UoM dev team to get your devkey, which is required to exchange data with UADI. Update the devkey in line in "uadi_indicator_YOUR_SCRIPT_NAME.R":

"myDevKey = "" # DO NOT CHANGE THIS VARIABLE NAME"

and save this file.

(8) install R packages on your dev environment. In RStudio, go to "Tools"->"Install Packages", make sure the "Install dependencies" option is checked, then install the following packages one by one:

'sp', maptools', 'rgdal', 'rgeos', 'jsonlite, 'geojsonio', 'RCurl', 'uuid', 'base64enc', 'digest'

you might need to close and reopen RStudio to use the installed packages


### How to run tests ###

(1) Launch cmd console, then type:

RScript YOUR_SCRIPT_DIRECTORY\uadi_indicator_YOUR_SCRIPT_NAME.R "JOBUUID_AS_YOUR_FIRST_PARAMETER" "YOUR_SECOND_PARAMETER" "YOUR_THIRD_PARAMETER" 

make sure when a url is used as parameter, it must be encoded(this online tool works great: https://www.urlencoder.org/) before being fed to the cmd.

You can also test your configuration by running this sample cmd:

RScript YOUR_SCRIPT_DIRECTORY\uadi_indicator_YOUR_SCRIPT_NAME.R "fake-job-uuid" "http%3A%2F%2F115.146.87.191%3A8080%2Fgeoserver%2Findi_ws_uom%2Fows%3Fservice%3DWFS%26version%3D1.0.0%26request%3DGetFeature%26typeName%3Dindi_ws_uom%3Aga_msu_55be885e-a9a7-4c1b-8c7c-55f73383e6c7%26outputFormat%3Djson" 666

If everything goes well, you will see console outputs like this:

[1] "=== outputs: {\"status\":0,\"errdesc\":\"\",\"data\":{CONTENT_TO_BE_SHOWN}}"

[1] "=== fake-job-uuid provided for testing. updateJob call skipped."

[1] TRUE 


(2) To obtain real data urls from UADI for testing, please visit:

 http://apps.csdila.ie.unimelb.edu.au/uadi/indicator/sampledata.html  
 

(3) Since jobuuid is not available during indicator tool development, we can assign it with string "fake-job-uuid" for testing (the "utils.updateJob" funciton will skip the outputs update process). We can use this line: 

utils.debugprint(toJSON(outputs, auto_unbox=TRUE))

to print the output in json format in console. Don't forget to comment this line once the script is ready for submission.

It is a good practice for testing the code piece by piece before assambling them into the formal "execIndicator_YOUR_INDICATOR_NAME" function. 
If you want to have a preview of the outputs visualisation, just copy the json structure data such as:

{	"status":0,
	"errdesc":"",
	"data":{"geolayers":[...], "tables":[...], "charts":[...]}
}

and validate the output with http://jsoneditoronline.org/ then paste it here:

http://apps.csdila.ie.unimelb.edu.au/uadi/indicator/preview.html

For more details about indicator tool outputs, check this link:

https://bitbucket.org/csdilaspatialtechteam/uadi-indicator-r-wrapper/wiki/Home 


### Who do I talk to? ###

If you have any questions or suggestions, please contact:

Dr Yiqun Chen

Centre for Disaster Management & Public Safety

Centre for Spatial Data Infrastructures & Land Administration

The University of Melbourne

E: yiqun.c@unimelb.edu.au