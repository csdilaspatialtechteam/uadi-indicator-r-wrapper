# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  Copyright 2016-2017 University of Melbourne
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Written by: Dr. Yiqun Chen    yiqun.c@unimelb.edu.au
#
# Run a test (make sure you get a vaild devKey and all prerequisites are met as described in the README of https://bitbucket.org/csdilaspatialtechteam/uadi-indicator-r-wrapper):
#
# RScript C:\Users\z8290362\Documents\uadi-indicator-R-wrapper\uadi_indicator_15_2.R "fake-job-uuid" http%3A%2F%2Fapps.csdila.ie.unimelb.edu.au%2Fuadi-service%2Fexecengine%2Fconcept%2Fgetfeatures%3Fdevkey%3D6267467f-7509-4516-b6a7-a852171d8b6f%26ontomapid%3D130%26ca%3D%7B%22caBuffer%22%3A-1.0E-5%2C%22caASGSCode%22%3A%2217200%22%2C%22caASGSType%22%3A%22lga%22%7D 

# 
#
#
# DevLogs:
#
# v2.0 2017-05-30 
# (1) assign devKey in this indicator script so that each developer can use their own devKey
# (2) new sample code for passing three paramters to the main execIndicatorSample function
#
# v1.3 2017-05-29 
# (1) use utils.getLayerNameFromWFSUrl and utils.getBbox functions to prepare output values
#
# v1.2 2017-05-19 
# (1) add sample codes for output wrapping
#
# v1.1 2017-04-19 
# (1) add LocationOfThisScript function to dynamically determine script location, so "uadi_utils.R" script can be sourced properly
#
# v1.0 2017-04-06
# (1) init implementation
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

library(maptools) 
library(rgdal)
library(rgeos)
library(jsonlite)

#### DO NOT CHANGE/DELETE THIS FUNCTION
LocationOfThisScript = function() # Function LocationOfThisScript returns the location of this .R script (may be needed to source other files in same dir)
{
  this.file = NULL
  # This file may be 'sourced'
  for (i in -(1:sys.nframe())) {
    if (identical(sys.function(i), base::source)) this.file = (normalizePath(sys.frame(i)$ofile))
  }
  
  if (!is.null(this.file)) return(dirname(this.file))
  
  # But it may also be called from the command line
  cmd.args = commandArgs(trailingOnly = FALSE)
  cmd.args.trailing = commandArgs(trailingOnly = TRUE)
  cmd.args = cmd.args[seq.int(from=1, length.out=length(cmd.args) - length(cmd.args.trailing))]
  res = gsub("^(?:--file=(.*)|.*)$", "\\1", cmd.args)
  
  # If multiple --file arguments are given, R uses the last one
  res = tail(res[res != ""], 1)
  if (0 < length(res)) return(dirname(res))
  
  # Both are not the case.
  return(NULL)
}

#### DO NOT CHANGE/DELETE THIS FUNCTION
setwd(LocationOfThisScript())


# ATTENTION:
# devKey is used for storing and publishing indicator outputs in UADI GeoServers so that the outputs can be viewed, used, downloaded by others
# To obtain a devKey for UADI indicator development, please contact UoM UADI dev team.

myDevKey = "" # DO NOT CHANGE THIS VARIABLE NAME

# this the main wrapper method which handles the arguments check and call the indicator calculation method
# to trigger this in cmd line, just run : RScript path\sample_code.R "arg1" "arg2"

# load utils methods for use
source("uadi_utils.R")

#### RENAME AND IMPLEMENT THIS FUNCTION, SUCH AS execIndicator_YOUR_INDICATOR_NAME, ALWAYS SET THE 1st PARAMETER AS "jobuuid"
#execIndicator_15_2 <- function(jobuuid, data_url, homeless_parm, pop_parm){
execIndicator_15_2 <- function(jobuuid, data_url){
  
  # check if myDevKey is set
  if(nchar(myDevKey)==0){
    utils.debugprint("devKey is not provided.")
    return(FALSE)
  }

  # ATTENTION: this function MUST be called first before calling any other utils functions
  utils.initGeoServerCredentials(myDevKey)
  
  # insert input parameter check logic here. make sure to convert data into right data type since by default the inputs are all treated as strings
  utils.debugprint(sprintf("para1: %s", jobuuid))
  utils.debugprint(sprintf("para2: %s", URLdecode(data_url)))
#  utils.debugprint(sprintf("para3: %s", homeless_parm))
#  utils.debugprint(sprintf("para4: %s", pop_parm))
  
  # the passed in url has to be url encoded otherwise '&' character will stop the Rscript parsing arguments properly
  # this will be guaranteed by the UADI system before sending the calculation request to the tool
  sp = utils.loadGeoJSON2SP(URLdecode(data_url))
  
  # check if data layer can be successfully loaded
  if(is.null(sp)){
    outputs = list(status=1, errdesc=sprintf("fail to load data layer:%s", URLdecode(data_url)))
    utils.debugprint(sprintf("outputs: %s",toJSON(outputs, auto_unbox=TRUE)))
    utils.updateJob(toJSON(outputs, auto_unbox=TRUE), jobuuid)
    return(FALSE)
  }
  
  # # # # # # # # # # # # # 
  # implement indicator logic here, such as
  # 1. spatial data operations like projection, intersection, union, 
  # 2. statistics generation
  # 3. etc.
  # # # # # # # # # # # # # 
  
  # initialise idxval values as 0.0
  sp@data[,"idxval"] = 0.0
  # build a filter to make sure equation only applies on valid values
  filter = !(is.na(sp@data[,"homelessCount"]) | is.na(sp@data[,"personCount"]) | sp@data[,"personCount"] == 0)
  # do the calculation
  sp@data[filter,"idxval"] = as.numeric(sp@data[filter,"homelessCount"]) / (as.numeric(sp@data[filter,"personCount"])/100000.0)
  
  # prepare attributes for publishing
  # show attribute names
  colnames(sp@data)
  # remove irrelevant attributes
  sp@data[,"hasExactGeometry"]=NULL
  sp@data[,"id"]=NULL
  sp@data[,"sa2_5digit_code"]=NULL
  
  colnames(sp@data)
  # make sure EVERY attribute name is shortened properly (<=10 chars); 
  # otherwise (if any attribute name is longer than 10 chars), writeOGR function will automatically abbrivate ALL attribute names longer than 7 chars to 7 chars (ugly names, this is an odd bug in writeOGR)
  colnames(sp@data) = c("hlcount","popcount","sa2_code11","sa2_name11","idxval")
  

  #########################################
  # Wrap up indicator outputs -- start
  #########################################
  # the output json MUST be organised in following structure:
  # { "status": 0,
  #	 "data" :{
  #		"geolayers":[
  #						{
  #							"layername":"WORKSPACE_NAME:DATALAYER_NAME",
  #							"layerdisplayname":"A_READABLE_NAME_FOR_YOUR_INDICATOR_OUTPUT_DATALAYER",
  #							"bbox":[],
  #							"wfs": {
  #								"url":"",
  #								#// ATTENTION: wfs style is NOT used in UADI currently, it is reserved for future development
  #								"styleparams":{ #// if provided, this can be used for automatic client-side style creation
  #									"attrname":"", #//attribute name that the style is created for
  #									"minval":min,
  #									"maxval":max,
  #									"meanval":mean,
  #									"sdval": standard deviation
  #									}
  #								},
  #							"wms": {
  #								"url":"http:#//xxxx/geoserver/WORKSPACE_NAME/wms",
  #								#//ATTENTION: wms style is mandatory and MUST be created so that the layer can be visualised in UADI.
  #								"styleparams":{
  #									"attrname":"", #//attribute name that the style is created for
  #									"stylename":"",
  #									"sldurl":"",
  #									"legendurl":""
  #									}
  #								}
  #						}
  #					],
  #   "textlines":[#//this structure is mainly designed for storing R console outputs (e.g. regression model)
  #               "line1","line2","line3","lineN"
  #         ],
  #		"tables":[#//this structure is complex enough to describe a matrix data like csv
  #					{
  #						"title": "Your Table Title",
  #						"data":[
  #					              {
  #					            	  "colname":"column1",
  #					            	  "values":[0,1,2,3,4,5]
  #					              },
  #					              {
  #					            	  "colname":"column2",
  #				            	  	  "values":["Azadeh","Sam","Benny","Soheil","Mohsen","Abbas"]
  #					              }
  #								]
  #					}		
  #		         ],
  #		"charts":[ #// this contains data structure for 8 types of charts supported by UADI including:
  #				   #// columnchart, barchart, linechart, areachart, radialchart, piechart, donutchart, scatterchart 
  #		              {
  #						  "type" : "columnchart", #// "barchart", "linechart", "areachart" and "radialchart" shares the exactly same data structure of "columnchart"
  #		            	  "seqnum":1, #// the visualisation sequence of charts 
  #						  "width":600, #// width of the chart, if not provided, default value on the visualisation page will be applied. (ATTENTION: UADI visualisation page will decide whether customised chart size is supported. There will be no effect if supporing customised chart size is disabled)
  #						  "height":400, #// height of the chart, if not provided, default value on the visualisation page will be applied. 
  #						  "xaxistitle":"", #//title for x axis
  #						  "yaxistitle":"", #//title for y axis
  #						  "xaxislabelsuffix":"", #//a suffix for labels on x axis, e.g., %, min, sec
  #						  "yaxislabelsuffix":"", #//a suffix for labels on y axis, e.g., %, min, sec
  #		            	  "title": "Your Chart Title", #// the title of chart
  #						  "stacked": false, #// whether to put yfileds on stack, this ONLY applies to "columnchart" and "barchart" when multiple yfields provided
  #						  "xfield": "month", #// a categorical attribute 
  #						  "yfield": ["data1", "data2", "data3", "data4"], #// an array of numeric y field(s), even with one element
  #						  "yfieldtitle": [ "IE", "Firefox", "Chrome", "Safari"], #// an array of title of y field(s) used in legend, even with one element  
  #						  "data": [#// prepared the data in json array format, xfield should be in the first column, the rest should be yfields 
  #					                { "month": "Jan", "data1": 20, "data2": 37, "data3": 35, "data4": 4 },
  #					                { "month": "Feb", "data1": 20, "data2": 37, "data3": 36, "data4": 5 },
  #					                { "month": "Mar", "data1": 19, "data2": 36, "data3": 37, "data4": 4 },
  #					                { "month": "Apr", "data1": 18, "data2": 36, "data3": 38, "data4": 5 },
  #					                { "month": "May", "data1": 18, "data2": 35, "data3": 39, "data4": 4 },
  #					                { "month": "Jun", "data1": 17, "data2": 34, "data3": 42, "data4": 4 },
  #					                { "month": "Jul", "data1": 16, "data2": 34, "data3": 43, "data4": 4 },
  #					                { "month": "Aug", "data1": 16, "data2": 33, "data3": 44, "data4": 4 },
  #					                { "month": "Sep", "data1": 16, "data2": 32, "data3": 44, "data4": 4 },
  #					                { "month": "Oct", "data1": 16, "data2": 32, "data3": 45, "data4": 4 },
  #					                { "month": "Nov", "data1": 15, "data2": 31, "data3": 46, "data4": 4 },
  #					                { "month": "Dec", "data1": 15, "data2": 31, "data3": 47, "data4": 4 }
  #					            ]
  #		              },
  #		              {
  #						  "type" : "piechart",  #//"donutchart" shares the exactly same data structure of "piechart"
  #		            	  "seqnum":2, #// the visualisation sequence of charts 
  #						  "width":600, #// width of the chart, if not provided, default value on the visualisation page will be applied. (ATTENTION: UADI visualisation page will decide whether customised chart size is supported. There will be no effect if supporing customised chart size is disabled)
  #						  "height":400, #// height of the chart, if not provided, default value on the visualisation page will be applied. 
  #		            	  "title": "Your Chart Title", #// the title of chart
  #						  "donut": false, #// set false to create a piechart, set true to create a donutchart
  #						  "xfield": "month", #// a categorical attribute 
  #						  "yfield": "data1", #// a numeric attribute 
  #						  "data": [#// prepared the data in json array format, xfield should be in the first column, the rest should be yfield 
  #									{ "month": "Jan", "data1": 68 },
  #									{ "month": "Feb", "data1": 17 },
  #									{ "month": "Mar", "data1": 15 }
  #					            ]
  #		              },
  #		              {
  #						  "type" : "scatterchart",
  #		            	  "seqnum":3, #// the visualisation sequence of charts 
  #						  "width":600, #// width of the chart, if not provided, default value on the visualisation page will be applied. (ATTENTION: UADI visualisation page will decide whether customised chart size is supported. There will be no effect if supporing customised chart size is disabled)
  #						  "height":400, #// height of the chart, if not provided, default value on the visualisation page will be applied. 
  #						  "xaxistitle":"", #//title for x axis
  #						  "yaxistitle":"", #//title for y axis
  #						  "xaxislabelsuffix":"", #//a suffix for labels on x axis, e.g., %, min, sec
  #						  "yaxislabelsuffix":"", #//a suffix for labels on y axis, e.g., %, min, sec
  #		            	  "title": "Your Chart Title", #// the title of chart
  #						  "xfield": "x", #// a numeric attribute 
  #						  "yfield": "y", #// a numeric attribute 
  #						  "data": [#// prepared the data in json array format 
  #									{ "x": 5, "y": 20 },
  #									{ "x": 480, "y": 90 },
  #									{ "x": 250, "y": 50 },
  #									{ "x": 100, "y": 33 },
  #									{ "x": 330, "y": 95 },
  #									{ "x": 410, "y": 12 },
  #									{ "x": 475, "y": 44 }
  #					            ]
  #		              }
  #		            ]
  #
  #
  #	},
  #   "errdesc": ""
  # }
  # "status" 0 means no error occurrs during the processing, 1 means processing has errors. Integer 0 or 1 (rather than string "0" or "1") is expected. 
  # "data" attribte can be either JSONObject (recommended) or JSONArray, it is the place where the outputs should be stored. 
  # "errdesc" contains the error descriptions, it should be empty when "status" sets to 0		
  
  # sample code
  # part 1: build a geolayers list to host every sp object as output.
  # a couple of steps are performed inside the "utils.publishGeolayer" method, including
  # (1)check the sp projection (EPSG:4326 (WGS84) will be converted to),
  # (2)push it geoserver
  # (3)create wms style for selected attributes
  # if everything goes well,a list containing the aforementioned data structure for geolayers will be returned, which then should be included in the final "geolayers" list
  
  
  # this example shows how to publish a geolayer by creating multiple wms styles on various attributes of the same data layer. 
  # the data layer will be only published one time, with various wms styles generated for selected attributes 
  geolayers_homelessratio = utils.publishGeolayerWithMultipleStyles(spobj=sp, 
                                                          displayname_vec=c("homeless ratio"),
                                                          attrname_vec=c("idxval"),
                                                          palettename_vec=c("Reds"), 
                                                          colorreverseorder_vec=c(FALSE), 
                                                          geomtype = "Geometry", 
                                                          colornum_vec=c(6), 
                                                          classifier_vec=c("Jenks")
  )
  
  if(is.null(geolayers_homelessratio) || length(geolayers_homelessratio)==0){
    outputs = list(status=1, errdesc="fail to save data to geoserver")
    utils.debugprint(sprintf("outputs: %s",toJSON(outputs, auto_unbox=TRUE)))
    utils.updateJob(toJSON(outputs, auto_unbox=TRUE), jobuuid)
    return(FALSE)
  }
  
  geolayers = list()
  geolayers = append(geolayers, geolayers_homelessratio)
  
  outputs = list(status=0, errdesc="", data=list(geolayers = geolayers))
  
  outputsJSONString = toJSON(outputs, auto_unbox=TRUE)
  
  
  #########################################
  # Wrap up indicator outputs -- end
  #########################################
  
  # for testing, just print the outputs in json format instead of calling utils.updateJob function
  utils.debugprint(sprintf("outputs: %s",outputsJSONString))
  
  # sync and update indicator job outputs (outputs has to be in json format)
  utils.updateJob(outputsJSONString, jobuuid)
  return(TRUE)
}

#### DO NOT CHANGE/DELETE THIS FUNCTION
args <- commandArgs(trailingOnly=TRUE)

#### CHANGE TO YOUR OWN FUNCTION NAME AND FEED IT WITH PROPER PARAMETERS 
execIndicator_15_2(jobuuid=args[1], data_url=args[2])
